CREATE TABLE `respuesta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idlinea` int NOT NULL,
  `idpregunta` int NOT NULL,
  `respuesta` varchar(500) NOT NULL,
  `valor` int NOT NULL,
  `fecha` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `respuesta_linea_idx` (`idlinea`),
  KEY `respuesta_pregunta_idx` (`idpregunta`),
  CONSTRAINT `respuesta_linea` FOREIGN KEY (`idlinea`) REFERENCES `lineaarchivo` (`id`),
  CONSTRAINT `respuesta_pregunta` FOREIGN KEY (`idpregunta`) REFERENCES `pregunta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=842 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
