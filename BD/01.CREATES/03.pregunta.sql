CREATE TABLE `pregunta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idtipopregunta` int NOT NULL,
  `pregunta` varchar(150) NOT NULL,
  `fecha` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pregunta_tipopregunta_idx` (`idtipopregunta`),
  CONSTRAINT `pregunta_tipopregunta` FOREIGN KEY (`idtipopregunta`) REFERENCES `tipopregunta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
