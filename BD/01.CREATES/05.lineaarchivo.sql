CREATE TABLE `lineaarchivo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idarchivo` int NOT NULL,
  `numero` int NOT NULL,
  `sumatoria` int NOT NULL,
  `fecha` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `linea_archivo_idx` (`idarchivo`),
  CONSTRAINT `linea_archivo` FOREIGN KEY (`idarchivo`) REFERENCES `archivo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
