CREATE TABLE `archivo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idusuario` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fechaalta` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archivo_usuario_idx` (`idusuario`),
  CONSTRAINT `archivo_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
