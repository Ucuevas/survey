INSERT INTO pregunta
(
			 idtipopregunta
			,pregunta
            ,fecha
            ,estatus
)
VALUES
(
			 1 /*CORREO*/
			,"¿Cuál es tu correo electrónico?"
            ,NOW()
            ,1
),
(
			 2 /*GÉNERO*/
			,"¿Cuál es tu género?"
            ,NOW()
            ,1
),
(
			 3 /*FECHA NACIMIENTO*/
			,"¿Cuál es tu fecha de nacimiento?"
            ,NOW()
            ,1
),
(
			 4 /* SI/NO */
			,"¿Quedó usted satisfecho con el servicio?"
            ,NOW()
            ,1
),
(
			 4 /* SI/NO */
			,"¿Consideraría volver a usar nuestro servicio?"
            ,NOW()
            ,1
),
(
			 5 /* 5 OPCIONES */
			,"Del 1 al 5. ¿Qué tan satisfecha quedó con nuestro servicio? Siendo 1 el menos satisfecho y 5 el más satisfecho"
            ,NOW()
            ,1
),
(
			 5 /* 5 OPCIONES */
			,"Del 1 al 5. ¿Qué tan posible es que recomiende nuestro servicio? Siendo 1 es nada probable y 5 es muy probable"
            ,NOW()
            ,1
),
(
			 6 /* 10 OPCIONES */
			,"Del 1 al 10. ¿Qué tan satisfecho quedó con el surtido en tienda? Siendo 1 nada satisfecho y 10 muy satisfecho"
            ,NOW()
            ,1
),
(
			 6 /* 10 OPCIONES */
			,"Del 1 al 10. ¿Qué tan satisfecho quedó con los métodos de pago? Siendo 1 nada satisfecho y 10 muy satisfecho"
            ,NOW()
            ,1
),
(
			 7 /* COMENTATIOS */
			,"Comentarios"
            ,NOW()
            ,1
)