DELIMITER $$
CREATE PROCEDURE `ValidaUsuario`(IN correo VARCHAR(100), IN contrasenia VARCHAR(100))
BEGIN
	/*DECLARACION DE VARIABLES*/
    SET @rowcount = 0;

	/*VALIDAMOS LOS DATOS DEL USUARIO*/
	SELECT		COUNT(0)
				INTO @rowcount
		FROM 	usuario u
        WHERE	u.correo		=	correo
				AND
                u.contrasenia	=	contrasenia
                AND
                estatus			=	1;
    
    /*SI NO SE ENCUENTRA EL USUARIO MANDAMOS MENSAJE*/
	IF @rowcount = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_NO_VALIDO';
    END IF;
    
    /*SI SE ENCUENTRA EL USUARIO, HACEMOS EL SELECT*/
    SELECT		 u.id
				,u.nombre
                ,u.correo
                ,u.fechaalta
		FROM	usuario u
		WHERE	u.correo		=	correo
				AND
                u.contrasenia	=	contrasenia
                AND
                estatus			=	1;
END$$
DELIMITER ;
