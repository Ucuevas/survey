DELIMITER $$
CREATE PROCEDURE `Respuesta_I`(
	 IN idlinea	INT
    ,IN idpregunta INT
    ,IN respuesta VARCHAR(500))
BEGIN

	/*DECLARACION DE VARIABLES*/
    SET @idtipo = 0;
    SET @valor = 0;

	/*OBTENEMOS EL TIPO DE PREGUNTA*/
    SELECT		p.idtipopregunta
				INTO @idtipo
		FROM	pregunta p
        WHERE	p.id	=	idpregunta;

	/*VALIDAMOS EL TIPO DE PREGUNTA PARA SACAR EL VALOR*/
    /*SI EL TIPO DE RESPUESTA ES SI/NO */
    IF @idtipo = 4 THEN
		/*SI LA RESPUESTA ES 1 (SI)*/
		IF respuesta = "1" THEN
			/*ENTONCES LA RESPUESTA TIENE UN VALOR DE 10 */
			SET @valor = 10;
        END IF;
    END IF;

	/*SI EL TIPO DE RESPUESTA ES DE 5 OPCIONES */
    IF @idtipo = 5 THEN
		SET @valor = 2 * CAST(respuesta AS UNSIGNED);
    END IF;

	/*SI EL TIPO DE RESPUESTA ES DE 10 OPCIONES */
    IF @idtipo = 6 THEN
		SET @valor = CAST(respuesta AS UNSIGNED);
    END IF;

	/*YA QUE SE CALCULÓ EL VALOR DE LA RESPUESTA SE INSERTA EL REGISTRO*/
    INSERT INTO respuesta
    (
				 idlinea
				,idpregunta
                ,respuesta
                ,valor
				,fecha
                ,estatus
    )
    VALUES
    (
				 idlinea
				,idpregunta
                ,respuesta
                ,@valor
                ,NOW()
                ,1
    );

	/*SI EL VALOR DE LA RESPUESTA ES MAYOR A CERO*/
    IF @valor > 0 THEN
		/*ACTUALIZAMOS EL VALOR DE LA LÍNEA SUMANDO EL VALOR DE LA RESPUESTA*/
        UPDATE		lineaarchivo
			SET		sumatoria 	= 	sumatoria + @valor
			WHERE	id			=	idlinea;
    END IF;

END$$
DELIMITER ;
