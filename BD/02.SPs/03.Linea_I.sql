DELIMITER $$
CREATE PROCEDURE `Linea_I`(
	 IN idarchivo INT
    ,IN numero INT)
BEGIN

	/*INSERTAMOS EL REGISTRO DE LA LINEA*/
	INSERT INTO lineaarchivo
    (
				 idarchivo
				,numero
                ,sumatoria
                ,fecha
                ,estatus
    )
    VALUES
    (
				 idarchivo
				,numero
                ,0
                ,NOW()
                ,1
    );

	/*OBTENEMOS EL MAXIMO ID RECIEN INSERTADO*/
	SELECT		MAX(l.id) AS id
		FROM	lineaarchivo l;

END$$
DELIMITER ;
