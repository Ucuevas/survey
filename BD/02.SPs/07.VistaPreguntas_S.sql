DELIMITER $$
CREATE PROCEDURE `VistaPreguntas_S`()
BEGIN
	
	SELECT		 p.id
				,p.pregunta
                ,t.descripcion
                ,SUM(r.valor) AS acumulado
                ,COUNT(r.id) AS lineas
                ,COUNT(r.id) * 10 AS limite
                ,(SUM(r.valor) * 100) / (COUNT(r.id) * 10) AS aceptacion
		FROM	pregunta p
				INNER JOIN tipopregunta t
					ON 	p.idtipopregunta	=	t.id
						AND
                        t.id				IN	(4,5,6)	
				INNER JOIN respuesta r
					ON 	p.id				=	r.idpregunta
						AND
                        r.estatus			=	1
		WHERE	p.estatus					=	1
        GROUP BY
				 p.id
				,p.pregunta
                ,t.descripcion;
        
    
END$$
DELIMITER ;
