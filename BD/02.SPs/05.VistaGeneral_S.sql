DELIMITER $$
CREATE PROCEDURE `VistaGeneral_S`()
BEGIN

	/*SELECCIONAMOS LOS ACUMULADOS GENERALES*/
	SELECT		 COUNT(l.id) AS lineas
                ,SUM(l.sumatoria) AS acumulado
                ,COUNT(l.id) * 60 AS limitetotal
                ,60 AS limiteporlinea
                ,SUM(l.sumatoria) / COUNT(l.id) AS promedio
                ,(SUM(l.sumatoria) * 100) / (COUNT(l.id) * 60) AS aceptacion
		FROM	archivo a
				INNER JOIN usuario u
					ON u.id 		=	a.idusuario
				INNER JOIN lineaarchivo l
					ON 	l.idarchivo	=	a.id
						AND
                        l.estatus	=	1
        WHERE	a.estatus			=	1;

	/*SELECCIONAMOS LOS RESULTADOS AGRUPADOS POR ARCHIVOS*/
	SELECT		 a.id
				,a.nombre
				,DATE_FORMAT(a.fechaalta,"%d/%m/%Y %H:%i" ) AS fechaalta
                ,u.nombre AS usuario
                ,COUNT(l.id) AS lineas
                ,SUM(l.sumatoria) AS acumulado
                ,COUNT(l.id) * 60 AS limitetotal
                ,60 AS limiteporlinea
                ,SUM(l.sumatoria) / COUNT(l.id) AS promedio
                ,(SUM(l.sumatoria) * 100) / (COUNT(l.id) * 60) AS aceptacion
		FROM	archivo a
				INNER JOIN usuario u
					ON u.id 		=	a.idusuario
				INNER JOIN lineaarchivo l
					ON 	l.idarchivo	=	a.id
						AND
                        l.estatus	=	1
        WHERE	a.estatus			=	1
        GROUP BY
				 a.id
				,a.nombre
				,a.fechaalta
                ,u.nombre;
        

        
    
    
END$$
DELIMITER ;
