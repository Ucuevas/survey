DELIMITER $$
CREATE PROCEDURE `Archivo_I`(IN nombre VARCHAR(100),IN idusuario INT)
BEGIN

	/*INSERTAMOS EL REGISTRO DEL ARCHIVO*/
	INSERT INTO archivo
    (
				 idusuario
                ,nombre
				,fechaalta
                ,estatus
    )
    VALUES
    (
				 idusuario
                ,nombre
				,NOW()
                ,1
    );
    
    /*SELECCIONAMOS EL ID MAYOR, RECIEN GENERADO CON EL INSERT*/
    SELECT		MAX(a.id) AS id
		FROM	archivo a;
    
END$$
DELIMITER ;
