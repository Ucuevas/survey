DELIMITER $$
CREATE PROCEDURE `VistaPorArchivo_S`(IN id INT)
BEGIN
	
    /*SELECCIONAMOS EL DETALLE DEL ARCHIVO*/
    SELECT		 l.id
				,l.numero
                ,l.sumatoria
                ,60 AS limite
                ,(l.sumatoria * 100) / 60 AS aceptacion
                ,DATE_FORMAT(l.fecha,"%d/%m/%Y %H:%i" ) AS fechaalta
		FROM	lineaarchivo l
        WHERE	l.idarchivo			=	id
        ORDER BY
				l.numero;
                
	/*SELECCIONAMOS LAS RESPUESTAS*/
    SELECT		 r.id
				,l.id as idlinea
                ,l.numero
				,p.pregunta
				,r.respuesta
				,r.valor
		FROM	respuesta r
				INNER JOIN pregunta p
					ON r.idpregunta		=	p.id
				INNER JOIN lineaarchivo l
					ON 	r.idlinea 		=	l.id
						AND
						l.idarchivo		=	id
		ORDER BY
				 l.numero
                ,r.id;
				    
END$$
DELIMITER ;
