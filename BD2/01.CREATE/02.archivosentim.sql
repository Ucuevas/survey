CREATE TABLE `archivosentim` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idusuario` int NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `polaridad` decimal(6,2) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `fechaalta` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archivosusuario_idx` (`idusuario`),
  CONSTRAINT `archivosusuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;