CREATE TABLE `respuestasentim` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idarchivosentim` int NOT NULL,
  `polaridad` decimal(6,2) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `fechaalta` datetime NOT NULL,
  `estatus` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archivosentimresp_idx` (`idarchivosentim`),
  CONSTRAINT `archivosentimresp` FOREIGN KEY (`idarchivosentim`) REFERENCES `archivosentim` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=673 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;