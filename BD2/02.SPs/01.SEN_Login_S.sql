DELIMITER $$
CREATE PROCEDURE `SEN_Login_S`(
	 IN correo 		VARCHAR(100)
	,IN contrasenia	VARCHAR(100)
)
BEGIN
	
    /*DECLARACION DE VARIABLES*/
    SET @reg_count = 0;
    
    /*VALIDACIONES*/
    SELECT		COUNT(0)
				INTO @reg_count
		FROM	usuario u
        WHERE	u.correo 		=	correo
				AND
                u.contrasenia	=	contrasenia
                AND
                u.estatus		=	1;
                
	/*SI NO SE ENCUENTRA EL USUARIO MANDAMOS LA EXCEPCIÓN*/
	IF @reg_count = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_NO_VALIDO';
    END IF;
    
    /*SI PASA EL IF ES QUE SI SE ENCUENTRA EL USUARIO Y HACEMOS EL SELECT*/
    SELECT		 u.id
				,u.nombre
				,u.correo
                ,u.fechaalta
		FROM	usuario u
        WHERE	u.correo		=	correo
				AND
                u.contrasenia	=	contrasenia
                AND
                u.estatus		=	1;
    
END$$
DELIMITER ;
