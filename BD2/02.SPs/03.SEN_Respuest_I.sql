DELIMITER $$
CREATE PROCEDURE `SEN_Respuesta_I`(
	 IN idarchivo INT
	,IN polaridad DECIMAL(6,2)
    ,IN tipo VARCHAR(45)
)
BEGIN
	
    INSERT INTO  respuestasentim
    (
				 idarchivosentim
				,polaridad
                ,tipo
                ,fechaalta
                ,estatus
    )
    VALUES
    (
				 idarchivo
				,polaridad
                ,tipo
                ,NOW()
                ,1
    );
   
	/*Seleccionamos el id del registro recien insertado*/
	SELECT		MAX(r.id) AS id
		FROM	respuestasentim r;
END$$
DELIMITER ;
