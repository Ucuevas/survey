DELIMITER $$
CREATE PROCEDURE `SEN_Archivo_I`(
	 IN idusuario INT
	,IN nombre VARCHAR(50)
    ,IN polaridad DECIMAL(6,2)
    ,IN tipo VARCHAR(45)
)
BEGIN
	
    INSERT INTO  archivosentim
    (
				 idusuario
				,nombre
                ,polaridad
                ,tipo
                ,fechaalta
                ,estatus
    )
    VALUES
    (
				 idusuario
				,nombre
                ,polaridad
                ,tipo
                ,NOW()
                ,1
    );
    
    /*SELECCIONAMOS EL ID RECIÉN GENERADO*/
    SELECT		MAX(a.id) AS id
		FROM	archivosentim a;
    
END$$
DELIMITER ;
