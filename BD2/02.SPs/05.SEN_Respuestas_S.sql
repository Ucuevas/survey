DELIMITER $$
CREATE PROCEDURE `SEN_Respuestas_S`(
	IN idarchivo INT
)
BEGIN
	
    /*Hacemos la consulta a los registros del archivo*/
    SELECT		 r.id
				,r.polaridad
                ,r.tipo
                ,DATE_FORMAT(r.fechaalta,"%d/%m/%Y %H:%i" ) AS fechaalta
		FROM	respuestasentim r
        WHERE	r.idarchivosentim	=	idarchivo
				AND
				r.estatus			=	1;
    
END$$
DELIMITER ;
