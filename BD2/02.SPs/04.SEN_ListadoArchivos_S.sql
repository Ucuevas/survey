DELIMITER $$
CREATE PROCEDURE `SEN_ListadoArchivos_S`()
BEGIN

	SELECT			 a.id
					,a.nombre AS archivo
					,u.nombre AS usuario
					,a.polaridad
                    ,a.tipo
                    ,DATE_FORMAT(a.fechaalta,"%d/%m/%Y %H:%i" ) AS fechaalta
                    ,COUNT(r.id) AS respuestas
		FROM		archivosentim a
					INNER JOIN usuario u
						ON a.idusuario			=	u.id
					INNER JOIN respuestasentim r
						ON r.idarchivosentim	=	a.id	
        WHERE		a.estatus					=	1
        GROUP BY
					 a.id
                    ,a.nombre
					,u.nombre
					,polaridad
                    ,tipo
                    ,a.fechaalta;
        

END$$
DELIMITER ;
