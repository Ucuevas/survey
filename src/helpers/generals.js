exports.messageBySqlException = function(sqlExMessage){
    switch(sqlExMessage){
        case "USUARIO_NO_VALIDO":
            return "Credenciales de usuario no válidas";
        default:
            return "No se pudo realizar el proceso, intenta más tarde";
    }
}

exports.validateDomain = function(email){
    var arr = email.split("@");
    if (arr[1] != "coppel.com"){
        return "Credenciales no válidas";
    }
    else{
        return "";
    }
}