const url = require("url");
const fs = require("fs");
const path = require('path');
const generals = require("../helpers/generals.js");
const config = require("../config.json");
const postRequest = require("request");
const surveyDAO = require("../dao/surveyDAO.js");

/*EXPOSICIÓN DE MÉTODOS*/
module.exports = {
     home : home
    ,login : login
    ,dashboard : dashboard
    ,postUploadFile : postUploadFile
    ,viewSentFile : viewSentFile
    ,viewSentDetail : viewSentDetail
}

function home(req,res){
    var html = fs.readFileSync(path.join(__dirname,"../views/") + "login.html","utf8");
    return res.send(html);
}

function login(req,res){
    surveyDAO.login(req.body.email,req.body.password,function(error,user){
        if(error){
            console.log(error);
            return res.status(400).json({code:-1,message: generals.messageBySqlException(error.sqlMessage)})
        }
        req.session.user = user;
        return res.redirect("/dashboard")
    })
}

function dashboard(req,res){
    if(req.session.user == undefined || req.session.user == null || req.session.user == ""){
        return res.redirect("/home");
    }
    surveyDAO.getSentFilesList(function(error,files){
        if(error){
            console.log(error);
            return res.status(400).json({code:-1,message: generals.messageBySqlException(error.sqlMessage)})
        }
        var rows = "";
        for(i=0;i<=files.length-1;i++){
            rows = rows + getFileRow(files[i]);
        }
        var html = fs.readFileSync(path.join(__dirname,"../views/") + "sentimupload.html","utf8");
        html = html.replace("{user}",req.session.user.nombre).replace("{rows}",rows);
        res.send(html);    
    });
    
}

function postUploadFile(req,res){
    var files = fs.readdirSync(path.join(__dirname, '../sentfiles'));
    console.log(files);
    processFile(files,req.session.user.id);
    return res.redirect("/dashboard");
}

function viewSentFile(req,res){
    var queryString = url.parse(req.url,true).query;
    try{
        var file = fs.readFileSync(path.join(__dirname,"../sentfilestemporary/") + queryString.filename);
        var html = fs.readFileSync(path.join(__dirname,"../views/") + "viewfile.html","utf8");
        html = html.replace("{filecontent}",file)
        return res.send(html);
    }
    catch(ex){
        return res.status(400).json({msg:"El archivo no se encuentra disponible"})
    }
}

function viewSentDetail(req,res){
    var queryString = url.parse(req.url,true).query;
    surveyDAO.getSentFileDetail(queryString.id,function(error,fileDetail){
        if(error){
            console.log(error);
            return res.status(400).json({code:-1,message: generals.messageBySqlException(error.sqlMessage)})
        }

        var regs = "";
        for(i=0;i<=fileDetail.length-1;i++){
            regs = regs + getDetailRow(fileDetail[i]);
        }

        var html = fs.readFileSync(path.join(__dirname,"../views/") + "sentfiledetail.html","utf8");
        html = html.replace("{rows}",regs)
        return res.send(html);

    });
}

/*MÉTODOS NO EXPUESTOS*/
async function processFile(files,userId){
    //Obtenemos las rutas y el contenido del archivo
    var filePath = path.join(__dirname, '../sentfiles') + "/" + files[0];
    var newFilePath = path.join(__dirname, '../sentfilestemporary') + "/" + files[0];
    var fileText = fs.readFileSync(filePath,"utf8");

    //Tiramos el POST al API y obtenemos su resultado para insertar el cabecero de la respuesta
    var postResult = await postAPI(fileText);
    var idNewFile = await surveyDAO.insertSentimFile(userId,files[0],postResult.result.polarity,postResult.result.type);
    
    //Hacemos ciclo para recorrer el resultado del POST al API
    var newAnswerId;
    for(j=0;j<=postResult.sentences.length-1;j++){
        //Insertamos la información de cada elemento de la respuesta del POST
        newAnswerId = await surveyDAO.insertSentimAnswer(idNewFile,postResult.sentences[j].sentiment.polarity,postResult.sentences[j].sentiment.type);
    }

    //Movemos el archivo de directorio y ponemos el temporizador 
    //para eliminar el archivo de la nueva ruta
    fs.renameSync(filePath,newFilePath);
    deleteTemporary(newFilePath);
}

let postAPI = function(fileText){
    return new Promise((resolve, reject) => {
        //Hacemos POST al API configurado
        postRequest({
            url: config.urlSentim
           ,method: "POST"
           ,json: true
           ,body: {text : fileText}
           ,headers : {
                "Accept": "application/json"
               ,"Content-Type" : "application/json"
           }
       },function(error,response2,response){
           if(error){
               reject(error);
               return;
           }
           else{
               resolve(response);
               return;
           }
       });
    });
}

function deleteTemporary(filePath){
    setTimeout(function(){
        fs.unlinkSync(filePath);  
    }, config.tempFileMinutes * 60000)
}

function getFileRow(file){
    return '<tr>' +
                '<td>' + file.archivo + '</td>' +
                '<td>' + file.usuario + '</td>' +
                '<td>' + file.polaridad + '</td>' +
                '<td>' + file.tipo + '</td>' +
                '<td>' + file.respuestas.toString() + '</td>' +
                '<td>' + file.fechaalta + '</td>' +
                '<td> <a href="viewsentfile?filename=' + file.archivo + '" target="_blank" >Ver Archivo</a><a href="viewsentdetail?id=' + file.id.toString() + '" target="_blank" >Ver Detalle</a></td>' +
            '</tr>'
}

function getDetailRow(reg){
    return '<tr>' +
                '<td>' + reg.polaridad.toString() + '</td>' +
                '<td>' + reg.tipo + '</td>' +
                '<td>' + reg.fechaalta + '</td>' +
            '</tr>'
}