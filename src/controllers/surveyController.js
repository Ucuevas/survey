const url = require("url");
const surveyDAO = require("../dao/surveyDAO.js")
const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const generals = require("../helpers/generals.js");
const { insertLine } = require("../dao/surveyDAO.js");
const config = require("../config.json");
const { query } = require("express");

/*EXPOSICIÓN DE MÉTODOS*/
module.exports = {
     getLanding : getLanding
    ,getUpload : getUpload
    ,postUploadFile : postUploadFile
    ,getResult : getResult
    ,getViewFile : getViewFile
    ,getViewDetail : getViewDetail
    ,getAnswerSurvey : getAnswerSurvey
    ,saveSurvey : saveSurvey
    ,getQuestionsResult : getQuestionsResult
}

function postUploadFile(req,res){
    /*VALIDAMOS EL DOMINIO DEL CORREO*/
    var validate = generals.validateDomain(req.body.email);
    if(validate == ""){
        /*VALIDAMOS CORREO Y PASSWORD*/
        surveyDAO.validateUser(req.body.email,req.body.password,function(error,result){
            if(error){
                return res.status(400).json({msg:"Usuario no válido"})
            }
            else{
                fs.readdir(path.join(__dirname, '../files'), (err, files) => {
                    processFile(files[0],result[0][0]);
                });
                return res.redirect("/")
            }
        });
    }
    else{
        console.log(validate);
        return res.status(400).json({msg:"Usuario no válido"})
    }
}

function getLanding(req,res){
    var html = fs.readFileSync(path.join(__dirname,"../views/") + "landing.html","utf8");
    res.send(html);
}

function getUpload(req,res){
    var html = fs.readFileSync(path.join(__dirname,"../views/") + "upload.html","utf8");
    res.send(html);
}

function getAnswerSurvey(req,res){
    //var html = fs.readFileSync(path.join(__dirname,"../views/") + "upload.html","utf8");
    htmlFile = path.join(__dirname,"../views/") + "survey.html"
    res.sendFile(htmlFile);
} 

function getResult(req,res){
    surveyDAO.getResults(function(error,results){
        if(error){
            console.log(error);
            return res.redirect("/");
        }

        var html = fs.readFileSync(path.join(__dirname,"../views/") + "resultadogeneral.html","utf8");
        var rows = "";
        html = html.replace("{totalencuestas}",results[0][0].lineas).replace("{totalacumulado}",results[0][0].acumulado)
                   .replace("{limitepuntosalcanzables}",results[0][0].limitetotal).replace("{promedioencuesta}",results[0][0].promedio)
                   .replace("{limiteporencuesta}",results[0][0].limiteporlinea).replace("{aceptaciongeneral}",results[0][0].aceptacion)

        for (i=0;i<=results[1].length-1;i++){
            rows = rows + getRegRow(results[1][i]);
        }
        html = html.replace("{rows}",rows);

        return res.send(html);


    });
}

function getViewFile(req,res){
    var queryString = url.parse(req.url,true).query;
    try{
        var file = fs.readFileSync(path.join(__dirname,"../files/temporary/") + queryString.name);
        var html = fs.readFileSync(path.join(__dirname,"../views/") + "viewfile.html","utf8");
        html = html.replace("{filecontent}",file)
        //file = path.join(__dirname,"../files/temporary/") + queryString.name;
        return res.send(html);
    }
    catch(err){
        //console.log(err);
        return res.status(400).json({msg:"El archivo no se encuentra disponible"})
    }
}

function getViewDetail(req,res){
    var queryString = url.parse(req.url,true).query;
    surveyDAO.getFileDetail(queryString.id,function(error,detail){
        if(error){
            console.log(error);
            return res.status(400).json({msg:"Hubo un error al consultar el detalle"})
        }
        var content = "";
        for(j=0;j<=detail[0].length-1;j++){
            content = content + getDetailHeader(detail[0][j]);
            content = content + getDetail(detail[0][j].id,detail[1]);
        }

        var html = fs.readFileSync(path.join(__dirname,"../views/") + "viewdetail.html","utf8");
        html = html.replace("{content}",content);
        return res.send(html);
    })
}

function saveSurvey(req,res){
    var fileName = "";
    var line = getLine(req.body);
    var currentDate = new Date();
    fs.readdir(path.join(__dirname, '../files/layouts'), (err, files) => {
        if(files.length==0){
            //Creamos el archivo
            fileName = currentDate.getFullYear() + "_" + currentDate.getMonth() + "_" + currentDate.getDate() + ".csv"
            var headers = "pregunta1,pregunta2,pregunta3,pregunta4,pregunta5,pregunta6,pregunta7,pregunta8,pregunta9,pregunta10";
            fs.writeFile(path.join(__dirname, '../files/layouts/') + fileName ,headers,function(error){
                if(error){
                    console.log(error);
                    return res.status(400).json({msg:"No se pudo generar el layout"});
                }
                //Escribimos la linea
                fs.appendFile (path.join(__dirname, '../files/layouts/') + fileName ,"\r\n" + line,function(error){
                    if(error){
                        console.log(error);
                        return res.status(400).json({msg:"No se pudo escribir en el layout"});
                    }
                    return res.redirect("/")
                });

            });
        }
        else{
            fileName = files[0];
            //Escribimos la linea
            fs.appendFile (path.join(__dirname, '../files/layouts/') + fileName ,"\r\n" +line,function(error){
                if(error){
                    console.log(error);
                    return res.status(400).json({msg:"No se pudo escribir en el layout"});
                }
                return res.redirect("/")
            });
        }
    });
}

function getQuestionsResult(req,res){
    surveyDAO.getQuestionsResult(function(error,questions){
        if(error){
            console.log(error);
            return res.status(400).json({msg:"No se pudieron obtener los datos de la base de datos"});
        }
        var html = fs.readFileSync(path.join(__dirname,"../views/") + "questionsdetail.html","utf8");
        var rows = "";
        for(i=0;i<=questions[0].length-1;i++){
            rows = rows + getQuestionRow(questions[0][i]);
        }
        html = html.replace("{rows}",rows);
        return res.send(html);
    });
}

/*METODOS LOCALES NO EXPUESTOS*/
async function processFile(fileName,user){
    //Validación para no subir la carpeta de archivos temporales
    if (fileName == "temporary"){
        //callback(null,{code:0})
        return 0;
    }
    else{
        var filePath = path.join(__dirname, '../files') + "/" + fileName;
        var newFilePath = path.join(__dirname, '../files/temporary') + "/" + fileName;
        const jsonObject = await csvtojson().fromFile(filePath);

        //Ya tenemos el archivo y ya tenemos el csv en json, insertamos a bd
        const idFile = await surveyDAO.insertFile(fileName,user.id);

        //Hacemos un ciclo para insertar las líneas
        for (i=0;i<=jsonObject.length-1;i++){
            //console.log(i,"jsonObject",jsonObject[i]);
            //Insertamos la línea
            let regLine = await insertLine(idFile[0][0].id,i+1)
                
            //Insertamos las respuestas de la línea
            await surveyDAO.insertAnswer(regLine[0][0].id,1,jsonObject[i].pregunta1)
            await surveyDAO.insertAnswer(regLine[0][0].id,2,jsonObject[i].pregunta2)
            await surveyDAO.insertAnswer(regLine[0][0].id,3,jsonObject[i].pregunta3)
            await surveyDAO.insertAnswer(regLine[0][0].id,4,jsonObject[i].pregunta4)
            await surveyDAO.insertAnswer(regLine[0][0].id,5,jsonObject[i].pregunta5)
            await surveyDAO.insertAnswer(regLine[0][0].id,6,jsonObject[i].pregunta6)
            await surveyDAO.insertAnswer(regLine[0][0].id,7,jsonObject[i].pregunta7)
            await surveyDAO.insertAnswer(regLine[0][0].id,8,jsonObject[i].pregunta8)
            await surveyDAO.insertAnswer(regLine[0][0].id,9,jsonObject[i].pregunta9)
            await surveyDAO.insertAnswer(regLine[0][0].id,10,jsonObject[i].pregunta10)
        }

        //Movemos el archivo de directorio
        fs.renameSync(filePath,newFilePath);
        deleteTemporary(newFilePath,function(err,deleted){})   
        return 0; 
    }
}

function deleteTemporary(filePath,callback){
    setTimeout(function(){
        fs.unlinkSync(filePath);  
    }, config.tempFileMinutes * 60000)
}

function getRegRow(reg){
    var row = '<tr>' +
                '<td>' +
                    reg.nombre +
                '</td>' +
                '<td>' +
                    reg.fechaalta +
                '</td>' +
                '<td>' +
                    reg.usuario +
                '</td>' +
                '<td>' +
                    reg.lineas.toString() +
                '</td>' +
                '<td>' +
                    reg.acumulado.toString() +
                '</td>' +
                '<td>' +
                    reg.limitetotal.toString() +
                '</td>' +
                '<td>' +
                    reg.promedio.toString() +
                '</td>' +
                '<td>' +
                    reg.limiteporlinea.toString() +
                '</td>' +
                '<td>' + 
                    reg.aceptacion.toString() +
                '</td>' +
                '<td>' + 
                    '<a href="viewfile?name=' + reg.nombre + '" target="_blank" >Ver Archivo</a> <a href="viewdetail?id=' + reg.id.toString() + '" target="_blank" >Ver Detalle</a>' +
                '</td>' +
            '</tr>'
    return row;
}

function getDetailHeader(reg){
    var ret = '<table>' + 
                '<tr>' +
                    '<td><b>Encuesta Número</b></td>' +
                    '<td><b>Puntos Acumulados</b></td>' +
                    '<td><b>Límite de Puntos</b></td>' +
                    '<td><b>Aceptación</b></td>' +
                    '<td><b>Fecha</b></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>' + reg.numero.toString() + '</td>' +
                    '<td>' + reg.sumatoria.toString() + '</td>' +
                    '<td>' + reg.limite + '</td>' +
                    '<td>' + reg.aceptacion.toString() + '</td>' +
                    '<td>' + reg.fechaalta + '</td>' +
                '</tr>' +
            '</table>'
    return ret;
}

function getDetail(idlinea,regs){
    var ret = '<table>' +
                '<tr>' +
                    '<td><b>Pregunta</b></td>' +
                    '<td><b>Respuesta</b></td>' +
                    '<td><b>Valor</b></td>' +
                '</tr>';
    
    for(i=0;i<=regs.length-1;i++){
        if(regs[i].idlinea == idlinea){
            ret = ret + '<tr>' +
                            '<td>' + regs[i].pregunta + '</td>' +
                            '<td>' + regs[i].respuesta + '</td>' +
                            '<td>' + regs[i].valor + '</td>' +
                        '</tr>'
        }
    }
    ret = ret + '</table>';

    return ret;
}

function getLine(body){
    line = body.Q1 + "," +
    body.Q2 + "," +
    body.Q3 + "," +
    body.Q4 + "," +
    body.Q5 + "," +
    body.Q6 + "," +
    body.Q7 + "," +
    body.Q8 + "," +
    body.Q9 + "," +
    body.Q10 
    return line;
}

function getQuestionRow(reg){
    var ret = '<tr>' +
                '<td>' + reg.pregunta + '</td>' +
                '<td>' + reg.descripcion + '</td>' +
                '<td>' + reg.acumulado + '</td>' +
                '<td>' + reg.lineas + '</td>' +
                '<td>' + reg.limite + '</td>' +
                '<td>' + reg.aceptacion + '</td>' +
            '</tr>'
    return ret;
}