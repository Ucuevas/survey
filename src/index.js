const oSurvey = require("./controllers/surveyController.js")
const oSentim = require("./controllers/sentimController.js")
const config = require("./config.json");
const express = require("express");
const session = require("express-session");
const myParser = require("body-parser");
const path = require("path");
var app = express();
var router = express.Router();
const multer = require("multer");
const storage = multer.diskStorage({
    destination : (req,file,callback) =>{
        callback( null,'./sentfiles')
    },
    filename : (req,file,callback) =>{
        //console.log(file);
        callback(null,file.originalname);
    }
});
const upload = multer({storage});

/*MÉTODOS DE CONTROLLER*/
router.route("/").get(oSurvey.getLanding);
router.route("/upload").get(oSurvey.getUpload);
router.route("/uploadFile").post(upload.array("files"),oSurvey.postUploadFile);
router.route("/results").get(oSurvey.getResult);
router.route("/viewfile").get(oSurvey.getViewFile);
router.route("/viewdetail").get(oSurvey.getViewDetail);
router.route("/answer").get(oSurvey.getAnswerSurvey);
router.route("/survey").post(oSurvey.saveSurvey);
router.route("/questions").get(oSurvey.getQuestionsResult);

/*MÉTODOS CONTROLLER SENTIM*/
router.route("/home").get(oSentim.home);
router.route("/login").post(oSentim.login);
router.route("/dashboard").get(oSentim.dashboard);
router.route("/uploadFileSen").post(upload.single("files"),oSentim.postUploadFile);
router.route("/viewsentfile").get(oSentim.viewSentFile);
router.route("/viewsentdetail").get(oSentim.viewSentDetail);

app.use(myParser.json());
app.use(session({secret:config.secret})); // para manejar sesiones
app.use(express.urlencoded({extended : false})); // para tomar los valores de los post
app.use('/', router);

//Puerto asignado por el server, si no, lo que hay en config
app.set('port', process.env.PORT || config.port);

app.listen(app.get("port"),function(){console.log("Started on PORT ", app.get("port"));});