const mysql = require("mysql");
const config = require("../config.json"); 

const dbConfig = {
    host     : config.mySQL.host,
    user     : config.mySQL.user,
    password : config.mySQL.password,
    database : config.mySQL.database};

function handleDisconnect() {
    connection = mysql.createConnection(dbConfig); // Recreate the connection, since
                                                    // the old one cannot be reused.
    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } 
        else{                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}
handleDisconnect();

function validateUser(email,password,callback){
    connection.query('CALL ValidaUsuario("' + email + '","' + password + '")',function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

let insertFile = function (fileName,idUser){
    return new Promise((resolve, reject) => {
        connection.query('CALL Archivo_I("' + fileName + '",' + idUser.toString() + ')',(error,result,fields) => {
            if(error){
                reject(error);
                return;
            }
            else{
                resolve(result);
                return;
            }
        });
    })
}

let insertLine = function (idFile,lineNumber){
    return new Promise((resolve, reject) => {
        connection.query('CALL Linea_I(' + idFile.toString() + ',' + lineNumber.toString() + ')',(error,result,fields) => {
            if(error){
                reject(error);
                return;
            }else{
                resolve(result);
                return;
            }
        });
    })
}

let insertAnswer = function(idLine,idQuestion,answer){
    return new Promise((resolve, reject) => {
        connection.query('CALL Respuesta_I(' + idLine.toString() + ',' + idQuestion.toString() + ',"' + answer + '")',function(error,result,fields){
            if(error){
                console.log(error);
                reject(error);
                return;
            }
            else{
                resolve(result);
                return;
            }
        })
    });
}

function getResults(callback){
    connection.query("CALL VistaGeneral_S()",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function getFileDetail(id,callback){
    connection.query("CALL VistaPorArchivo_S(" + id.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function getQuestionsResult(callback){
    connection.query("CALL VistaPreguntas_S()",function(error,result,fields){
        if(error){
            callback(error);
            return
        }
        callback(null,result);
        return;
    });
}

function login(email,password,callback){
    connection.query('CALL SEN_Login_S("' + email + '","' + password + '")',function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result[0][0]);
        return;
    });
}

let insertSentimFile = function(userId,fileName,polarity,type){
    return new Promise((resolve, reject) => {
        connection.query('CALL SEN_Archivo_I(' + userId.toString() + ',"' + fileName + '",' + polarity.toString() + ',"' +  type+ '");',function(error,result,fields){
            if(error){
                reject(error);
                return;
            }
            resolve(result[0][0].id);
            return;
        });
    });
}

let insertSentimAnswer = function(idFile,polarity,type){
    return new Promise((resolve, reject) => {
        connection.query('CALL SEN_Respuesta_I(' + idFile.toString() + ',' + polarity.toString() + ',"' + type + '")',function(error,result,fields){
            if(error){
                reject(error);
                return;
            }
            resolve(result[0][0].id);
            return;
        });
    });
}

function getSentFilesList(callback){
    connection.query("CALL SEN_ListadoArchivos_S()",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result[0]);
        return;
    });
}

function getSentFileDetail(id,callback){
    connection.query('CALL SEN_Respuestas_S(' + id.toString() + ')',function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result[0]);
        return;
    });
}

module.exports = {
    validateUser : validateUser
   ,insertFile : insertFile
   ,insertLine : insertLine
   ,insertAnswer : insertAnswer
   ,getResults : getResults
   ,getFileDetail : getFileDetail
   ,getQuestionsResult : getQuestionsResult
   ,login : login
   ,insertSentimFile : insertSentimFile
   ,insertSentimAnswer : insertSentimAnswer
   ,getSentFilesList : getSentFilesList
   ,getSentFileDetail : getSentFileDetail
}